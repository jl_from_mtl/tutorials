**Lead contributors:**

**Sharib Ali** <sharib.ali@eng.ox.ac.uk>, and 

**Avelino Javer** <avelino.javer@eng.ox.ac.uk>


#### Basics on usages: What you will find?

- [Conda basics and building virtual envs](https://gitlab.com/sharibOx/tutorials/blob/master/conda_essentials.md)

- [Docker deployment](https://gitlab.com/sharibOx/tutorials/blob/master/docker_essentials.md) 

- [Cluster rescompX usage basic](https://gitlab.com/sharibOx/tutorials/blob/master/clusterRescomp.md) 

- [Cluster rescompX gpu usage](https://gitlab.com/sharibOx/tutorials/blob/master/buildMyGpuEnvironment.md)

- [Buidling your own softwares and dependencies using cmake/gmake](https://gitlab.com/sharibOx/tutorials/blob/master/buildyourDependencies.md)

- [Manage your data and software builds for share](https://gitlab.com/sharibOx/tutorials/blob/master/putmyDataResComp.md)






	


